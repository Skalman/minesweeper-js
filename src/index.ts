import Root from "./ui/Root.tsx";
import { ReactDOM } from "./deps.ts";

window.onload = () => {
  ReactDOM.render(Root(), document.body);
};
