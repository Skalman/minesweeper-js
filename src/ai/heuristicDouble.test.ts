import { Heuristic } from "./getAiActions.ts";
import testWithTransformations from "./testWithTransformations.test.ts";

Deno.test("adjacent untouched fields, adjacent target field", () => {
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      C#C
      11C
    `,
    expected: `
      C#1
      111
  `,
  });
});

Deno.test("adjacent untouched fields, target field across", () => {
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      1C1
      1#C
    `,
    expected: `
      1C1
      1#1
    `,
  });
});

Deno.test(
  "adjacent untouched fields, adjacent target field has flagged neighbor",
  () => {
    testWithTransformations({
      heuristic: Heuristic.openDouble,
      original: `
        CF
        #2
        C1
      `,
      expected: `
        2F
        #2
        C1
      `,
    });
  }
);

Deno.test("untouched fields across, target field in between", () => {
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      11111
      C#1C#
      CCCCC
    `,
    expected: `
      11111
      C#1C#
      C111C
    `,
  });
});

Deno.test("untouched fields across, target field on other side", () => {
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      CCC#CCC
      CCCCCCC
      C#212#C
      CC#1C#C
      #21112#
    `,
    expected: `
      CCC#CCC
      CC212CC
      C#212#C
      CC#1C#C
      #21112#
    `,
  });
});

Deno.test("untouched fields diagonally adjacent", () => {
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      1#C
      C1C
    `,
    expected: `
      1#1
      C11
    `,
  });
});

Deno.test("flag others when none is already flagged", () => {
  testWithTransformations({
    heuristic: Heuristic.flagDouble,
    original: `
        C##
        13#
      `,
    expected: `
        C#F
        13F
    `,
  });
});

Deno.test("flag others when one is already flagged", () => {
  testWithTransformations({
    heuristic: Heuristic.flagDouble,
    original: `
        C#F
        13#
      `,
    expected: `
        C#F
        13F
    `,
  });
});

Deno.test("no actions - untouched fields too far apart", () => {
  // The bottom middle field is being tested here.
  testWithTransformations({
    heuristic: Heuristic.openDouble,
    original: `
      CCCCC
      #C11C
      C11#C
    `,
    expected: `
      CCCCC
      #C11C
      C11#C
    `,
  });
});
