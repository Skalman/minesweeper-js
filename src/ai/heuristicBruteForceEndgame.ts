import combinations from "../util/combinations.ts";
import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicBruteForceEndgame: HeuristicFunc<{
  open: Set<MineField>;
  flag: Set<MineField>;
  completeAnalysis: boolean;
}> = (map) => {
  const sets = {
    open: new Set<MineField>(),
    flag: new Set<MineField>(),
    completeAnalysis: false,
  };

  // TODO Intelligently ignore untouched fields without open neighbors.
  if (map.untouchedCount > 30) {
    return sets;
  }

  // TODO Reuse this code for the normal brute force.
  const untouchedNeighbors = map.untouched;
  const toFlagCount = map.mines.length - map.flags.length;

  const openNeighborsOfUntouchedFields = new Set(
    untouchedNeighbors.flatMap((x) => x.openNeighbors)
  );

  let acceptableToFlag: readonly MineField[] | undefined;
  let acceptableToOpen: readonly MineField[] | undefined;
  let iterationCount = 0;

  combinations: for (const toFlag of combinations(
    untouchedNeighbors,
    toFlagCount
  )) {
    iterationCount++;
    if (iterationCount > 5000) {
      // Break in order to not take too much time.
      return sets;
    }

    const toOpen = MineField.difference(untouchedNeighbors, toFlag);

    for (const {
      untouchedNeighbors,
      toFlagCount,
      toOpenCount,
    } of openNeighborsOfUntouchedFields) {
      const isAcceptable =
        MineField.intersectionCount(toFlag, untouchedNeighbors) <=
          toFlagCount &&
        MineField.intersectionCount(toOpen, untouchedNeighbors) <= toOpenCount;

      if (!isAcceptable) {
        continue combinations;
      }
    }

    acceptableToFlag = acceptableToFlag
      ? MineField.intersect(acceptableToFlag, toFlag)
      : toFlag;

    acceptableToOpen = acceptableToOpen
      ? MineField.intersect(acceptableToOpen, toOpen)
      : toOpen;

    if (acceptableToFlag.length === 0 && acceptableToOpen.length === 0) {
      break combinations;
    }
  }

  if (acceptableToFlag) {
    for (const toFlag of acceptableToFlag) {
      sets.flag.add(toFlag);
    }
  }

  if (acceptableToOpen) {
    for (const toOpen of acceptableToOpen) {
      sets.open.add(toOpen);
    }
  }

  sets.completeAnalysis = true;
  return sets;
};

export default heuristicBruteForceEndgame;
