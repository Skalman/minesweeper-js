import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicRandomizeInitial: HeuristicFunc<MineField | undefined> = (
  map
) => {
  const MAX_RANDOMIZE_COUNT = 10;

  if (map.open.length > MAX_RANDOMIZE_COUNT || map.flags.length > 0) {
    return;
  }

  for (const field of map.open) {
    if (field.openNeighborsCount > 0) {
      return;
    }
  }

  const [width, height] = map.size;
  if (width * height <= MAX_RANDOMIZE_COUNT) {
    return;
  }

  while (true) {
    const randomX = Math.floor(Math.random() * width);
    const randomY = Math.floor(Math.random() * height);

    const field = map.get(randomX, randomY);
    if (!field.isOpen) {
      return field;
    }
  }
};

export default heuristicRandomizeInitial;
