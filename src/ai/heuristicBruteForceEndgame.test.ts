import { Heuristic } from "./getAiActions.ts";
import testWithTransformations from "./testWithTransformations.test.ts";

Deno.test("flags single field", () => {
  testWithTransformations({
    heuristic: Heuristic.flagBruteForceEndgame,
    original: `
      12F
      C#2
      CC1
    `,
    expected: `
      12F
      CF2
      CC1
    `,
  });
});

Deno.test("doesn't flag", () => {
  testWithTransformations({
    heuristic: Heuristic.flagBruteForceEndgame,
    original: `
      12F
      #C2
      C#1
    `,
    expected: `
      12F
      #C2
      C#1
    `,
  });
});

Deno.test("flags three", () => {
  testWithTransformations({
    heuristic: Heuristic.flagBruteForceEndgame,
    original: `
      12F
      #C2
      ##1
    `,
    expected: `
      12F
      FC2
      FF1
    `,
  });
});

Deno.test("opens", () => {
  testWithTransformations({
    heuristic: Heuristic.openBruteForceEndgame,
    original: `
      12F
      #C2
      ##1
    `,
    expected: `
      12F
      #32
      ##1
    `,
  });
});

Deno.test("opens when not in two groups", () => {
  testWithTransformations({
    heuristic: Heuristic.openBruteForceEndgame,
    original: `
      12F21
      C#2C#
      CCCCC
    `,
    expected: `
      12F21
      C#2C#
      11121
    `,
  });
});

Deno.test("determines completely", () => {
  testWithTransformations({
    heuristic: Heuristic.openBruteForceEndgame,
    original: `
      12F22F1
      C#2C#21
      CCCCC10
    `,
    expected: `
      12F22F1
      1#22#21
      1112110
    `,
  });
});

Deno.test("flags single in large map", () => {
  testWithTransformations({
    heuristic: Heuristic.flagBruteForceEndgame,
    original: `
      1F11F1000
      112332000
      122FF1000
      F2F321000
      121100000
      011100122
      12F1001FF
      C#2100122
      CC1000000
    `,
    expected: `
      1F11F1000
      112332000
      122FF1000
      F2F321000
      121100000
      011100122
      12F1001FF
      CF2100122
      CC1000000
    `,
  });
});

Deno.test("open rest in large map", () => {
  testWithTransformations({
    heuristic: Heuristic.openBruteForceEndgame,
    original: `
      001F101F1
      001110111
      221000000
      FF2211000
      23F2F1000
      022322100
      12F22F100
      C#2C#2100
      CCCCC1000
    `,
    expected: `
      001F101F1
      001110111
      221000000
      FF2211000
      23F2F1000
      022322100
      12F22F100
      1#22#2100
      111111000
    `,
  });
});

Deno.test("open rest in large map", () => {
  testWithTransformations({
    heuristic: Heuristic.flagBruteForce,
    original: `
      #########
      #########
      #########
      233333332
    `,
    expected: `
      #########
      #########
      FFFFFFFFF
      233333332
    `,
    // TODO
    //   expected: `
    //     FFFFFFFFF
    //     FFFFFFFFF
    //     FFFFFFFFF
    //     233333332
    // `,
  });
});
