import combinations from "../util/combinations.ts";
import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicBruteForce: HeuristicFunc<{
  open: Set<MineField>;
  flag: Set<MineField>;
}> = (map) => {
  const sets = {
    open: new Set<MineField>(),
    flag: new Set<MineField>(),
  };

  for (const field of map.open) {
    const { untouchedNeighbors, toFlagCount } = field;

    if (untouchedNeighbors.length === 0) {
      continue;
    }

    const openNeighborsOfUntouchedNeighbors = new Set(
      untouchedNeighbors.flatMap((x) => x.openNeighbors)
    );

    let acceptableToFlag: readonly MineField[] | undefined;
    let acceptableToOpen: readonly MineField[] | undefined;

    combinations: for (const toFlag of combinations(
      untouchedNeighbors,
      toFlagCount
    )) {
      const toOpen = MineField.difference(untouchedNeighbors, toFlag);

      for (const {
        untouchedNeighbors,
        toFlagCount,
        toOpenCount,
      } of openNeighborsOfUntouchedNeighbors) {
        const isAcceptable =
          MineField.intersectionCount(toFlag, untouchedNeighbors) <=
            toFlagCount &&
          MineField.intersectionCount(toOpen, untouchedNeighbors) <=
            toOpenCount;

        if (!isAcceptable) {
          continue combinations;
        }
      }

      acceptableToFlag = acceptableToFlag
        ? MineField.intersect(acceptableToFlag, toFlag)
        : toFlag;

      acceptableToOpen = acceptableToOpen
        ? MineField.intersect(acceptableToOpen, toOpen)
        : toOpen;

      if (acceptableToFlag.length === 0 && acceptableToOpen.length === 0) {
        break combinations;
      }
    }

    if (acceptableToFlag) {
      for (const toFlag of acceptableToFlag) {
        sets.flag.add(toFlag);
      }
    }

    if (acceptableToOpen) {
      for (const toOpen of acceptableToOpen) {
        sets.open.add(toOpen);
      }
    }
  }

  return sets;
};

export default heuristicBruteForce;
