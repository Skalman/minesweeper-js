import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicGameOver: HeuristicFunc<boolean> = (map) => {
  const [width, height] = map.size;
  const { mines, open, flags } = map;

  const isExploded = MineField.intersect(mines, open).length !== 0;

  const isAllFlagged = mines.length === flags.length;

  const isAllOpen = width * height - open.length === mines.length;

  return isExploded || isAllFlagged || isAllOpen;
};

export default heuristicGameOver;
