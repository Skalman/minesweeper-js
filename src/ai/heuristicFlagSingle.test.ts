import { Heuristic } from "./getAiActions.ts";
import testWithTransformations from "./testWithTransformations.test.ts";

Deno.test("flags single field", () => {
  testWithTransformations({
    heuristic: Heuristic.flagSingle,
    original: `
      #1
    `,
    expected: `
      F1
    `,
  });
});

Deno.test("does not flag when everything is closed", () => {
  testWithTransformations({
    heuristic: Heuristic.flagSingle,
    original: `
      #C
    `,
    expected: `
      #C
    `,
  });
});
