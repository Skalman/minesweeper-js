import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicOpenSingle: HeuristicFunc = (map) => {
  const set = new Set<MineField>();

  for (const field of map.open) {
    if (field.mineNeighbors.length === field.flaggedNeighborCount) {
      for (const neighbor of field.untouchedNeighbors) {
        set.add(neighbor);
      }
    }
  }

  return set;
};

export default heuristicOpenSingle;
