import heuristicBruteForce from "./heuristicBruteForce.ts";
import heuristicBruteForceEndgame from "./heuristicBruteForceEndgame.ts";
import heuristicDouble from "./heuristicDouble.ts";
import heuristicFlagSingle from "./heuristicFlagSingle.ts";
import heuristicGameOver from "./heuristicGameOver.ts";
import heuristicOpenSingle from "./heuristicOpenSingle.ts";
import heuristicRandomizeInitial from "./heuristicRandomizeInitial.ts";
import { MineField, MineMap } from "../models/models.ts";
import { State, Position } from "../models/types.ts";

type Operation = "flag" | "open";

/**
 * Legend
 *
 * - 🚩 - Flagged and has a mine
 * - 💣 - Mine, not opened
 * - 🔲 - Closed
 * - 🟦 - Open, no neigboring mines
 * - 1️⃣2️⃣3️⃣4️⃣ - Opened field, number of neigboring mines
 * - ❔
 */
export enum Heuristic {
  all = "all",
  /**
   * Detect whether the game has ended.
   */
  gameOver = "gameOver",
  /**
   * When the game starts, randomize a few clicks, since it's impossible to
   * know where the mines are.
   */
  randomizeInitial = "randomizeInitial",
  /**
   * ```
   * 🚩1️⃣  >  🚩1️⃣
   * 🔲🔲  >  🟦🟦
   * ```
   */
  openSingle = "openSingle",
  /**
   * ```
   * 🚩3️⃣  >  🚩3️⃣
   * 🔲🔲  >  🚩🚩
   * ```
   */
  flagSingle = "flagSingle",
  /**
   * ```
   * 1️⃣1️⃣🔲  >  1️⃣1️⃣🟦
   * 🔲🔲🔲  >  🔲🔲🟦
   * ```
   */
  openDouble = "openDouble",
  /**
   * ```
   * 1️⃣3️⃣🔲  >  1️⃣1️⃣🚩
   * 🔲🔲🔲  >  🔲🔲🚩
   * ```
   */
  flagDouble = "flagDouble",

  openBruteForce = "openBruteForce",
  flagBruteForce = "flagBruteForce",

  openBruteForceEndgame = "openBruteForceEndgame",
  flagBruteForceEndgame = "flagBruteForceEndgame",
}

export interface AiAction {
  op: Operation;
  pos: Position;
  heuristic: Heuristic;
}

let mineMap: MineMap | undefined;

const getAiActions = (state: State, heuristic = Heuristic.all): AiAction[] => {
  if (!mineMap) {
    mineMap = new MineMap(state);
  } else {
    mineMap.updateState(state);
  }

  const all = heuristic === Heuristic.all;

  if (all || heuristic === Heuristic.gameOver) {
    const isGameOver = heuristicGameOver(mineMap);
    if (isGameOver) {
      return [];
    }
  }

  const results: AiAction[] = [];
  const addFlags = (heuristic: Heuristic, fields: Iterable<MineField>) => {
    for (const { x, y } of fields) {
      results.push({ op: "flag", pos: [x, y], heuristic });
    }
  };
  const addOpens = (heuristic: Heuristic, fields: Iterable<MineField>) => {
    for (const { x, y } of fields) {
      results.push({ op: "open", pos: [x, y], heuristic });
    }
  };

  if (all || heuristic === Heuristic.flagSingle) {
    addFlags(Heuristic.flagSingle, heuristicFlagSingle(mineMap));
  }

  if (all || heuristic === Heuristic.openSingle) {
    addOpens(Heuristic.openSingle, heuristicOpenSingle(mineMap));
  }

  if (
    all ||
    heuristic === Heuristic.openDouble ||
    heuristic === Heuristic.flagDouble
  ) {
    const { open, flag } = heuristicDouble(mineMap);

    if (all || heuristic === Heuristic.openDouble) {
      addOpens(Heuristic.openDouble, open);
    }

    if (all || heuristic === Heuristic.flagDouble) {
      addFlags(Heuristic.flagDouble, flag);
    }
  }

  if (
    all ||
    heuristic === Heuristic.openBruteForce ||
    heuristic === Heuristic.flagBruteForce
  ) {
    const { open, flag } = heuristicBruteForce(mineMap);

    if (all || heuristic === Heuristic.openBruteForce) {
      addOpens(Heuristic.openBruteForce, open);
    }

    if (all || heuristic === Heuristic.flagBruteForce) {
      addFlags(Heuristic.flagBruteForce, flag);
    }
  }

  if (
    all ||
    heuristic === Heuristic.openBruteForceEndgame ||
    heuristic === Heuristic.flagBruteForceEndgame
  ) {
    const { open, flag } = heuristicBruteForceEndgame(mineMap);

    if (all || heuristic === Heuristic.openBruteForceEndgame) {
      addOpens(Heuristic.openBruteForceEndgame, open);
    }

    if (all || heuristic === Heuristic.flagBruteForceEndgame) {
      addFlags(Heuristic.flagBruteForceEndgame, flag);
    }
  }

  if (!results.length) {
    if (all || heuristic === Heuristic.randomizeInitial) {
      const field = heuristicRandomizeInitial(mineMap);
      if (field) {
        addOpens(Heuristic.randomizeInitial, [field]);
      }
    }
  }

  return results;
};

export default getAiActions;
