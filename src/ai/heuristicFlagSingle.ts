import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicFlagSingle: HeuristicFunc = (map) => {
  const set = new Set<MineField>();

  for (const field of map.open) {
    if (field.mineNeighbors.length === field.closedNeighbors.length) {
      for (const neighbor of field.untouchedNeighbors) {
        set.add(neighbor);
      }
    }
  }

  return set;
};

export default heuristicFlagSingle;
