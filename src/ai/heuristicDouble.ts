import { MineField } from "../models/models.ts";
import { HeuristicFunc } from "../models/types.ts";

const heuristicOpenDouble: HeuristicFunc<{
  open: Set<MineField>;
  flag: Set<MineField>;
}> = (map) => {
  const sets = {
    open: new Set<MineField>(),
    flag: new Set<MineField>(),
  };

  const getSafe = (x: number, y: number) => {
    try {
      return map.get(x, y);
    } catch {
      return undefined;
    }
  };

  for (const field of map.open) {
    const precondition =
      // Two untouched neighbors
      field.untouchedNeighbors.length === 2 &&
      // ...where one should be flagged, the other opened
      field.mineNeighbors.length - field.flaggedNeighborCount === 1;
    if (!precondition) {
      continue;
    }

    const [untouchedNeighbor1, untouchedNeighbor2] = field.untouchedNeighbors;

    const xDistance = Math.abs(untouchedNeighbor1.x - untouchedNeighbor2.x);
    const yDistance = Math.abs(untouchedNeighbor1.y - untouchedNeighbor2.y);

    if (xDistance + yDistance > 2) {
      continue;
    }

    let fieldsWithUntouchedNeighborsInCommon: (MineField | undefined)[];

    if (xDistance === 1 && yDistance === 1) {
      // Diagonally adjacent
      // 🔲❔
      // 1️⃣🔲

      // The target coordinate has X in common with one untouched neighbor,
      // and Y in common with the other.
      const targetPos =
        untouchedNeighbor1.x === field.x
          ? getSafe(untouchedNeighbor2.x, untouchedNeighbor1.y)
          : getSafe(untouchedNeighbor1.x, untouchedNeighbor2.y);

      fieldsWithUntouchedNeighborsInCommon = [targetPos];
    } else {
      // Adjacent or one away
      // 🔲🔲        🔲❔🔲

      // The target coordinate has X or Y in common with the current field,
      // and the diff in the other extent is 2.
      if (xDistance > 0) {
        // Horizontally
        const yDiff = untouchedNeighbor1.y - field.y;
        const yOther = field.y + yDiff * 2;

        if (xDistance === 1) {
          // 🔲🔲
          const xOtherUntouched =
            field.x === untouchedNeighbor1.x
              ? untouchedNeighbor2.x
              : untouchedNeighbor1.x;

          fieldsWithUntouchedNeighborsInCommon = [
            getSafe(untouchedNeighbor1.x, yOther),
            getSafe(untouchedNeighbor2.x, yOther),
            getSafe(xOtherUntouched, field.y),
          ];
        } else {
          // 🔲❔🔲
          fieldsWithUntouchedNeighborsInCommon = [
            getSafe(field.x, yOther),
            getSafe(field.x, untouchedNeighbor1.y),
          ];
        }
      } else {
        // Vertically
        const xDiff = untouchedNeighbor1.x - field.x;
        const xOther = field.x + xDiff * 2;

        if (yDistance === 1) {
          // 🔲
          // 🔲
          const yOtherUntouched =
            field.y === untouchedNeighbor1.y
              ? untouchedNeighbor2.y
              : untouchedNeighbor1.y;

          fieldsWithUntouchedNeighborsInCommon = [
            getSafe(xOther, untouchedNeighbor1.y),
            getSafe(xOther, untouchedNeighbor2.y),
            getSafe(field.x, yOtherUntouched),
          ];
        } else {
          // 🔲
          // ❔
          // 🔲
          fieldsWithUntouchedNeighborsInCommon = [
            getSafe(xOther, field.y),

            getSafe(untouchedNeighbor1.x, field.y),
          ];
        }
      }
    }

    for (const other of fieldsWithUntouchedNeighborsInCommon) {
      if (
        !other ||
        !other.isOpen ||
        other.closedNeighbors.length === 0 ||
        // Need more than 2 untouched fields.
        other.untouchedNeighbors.length <= 2
      ) {
        continue;
      }

      const neighborsToFlag =
        other.mineNeighbors.length - other.flaggedNeighborCount;
      if (neighborsToFlag === 1) {
        const neighborsToOpen = other.untouchedNeighbors.filter(
          (x) => x !== untouchedNeighbor1 && x !== untouchedNeighbor2
        );
        for (const x of neighborsToOpen) {
          sets.open.add(x);
        }
      } else if (other.untouchedNeighbors.length - neighborsToFlag === 1) {
        const neighborsToFlag = other.untouchedNeighbors.filter(
          (x) => x !== untouchedNeighbor1 && x !== untouchedNeighbor2
        );
        for (const x of neighborsToFlag) {
          sets.flag.add(x);
        }
      }
    }
  }

  return sets;
};

export default heuristicOpenDouble;
