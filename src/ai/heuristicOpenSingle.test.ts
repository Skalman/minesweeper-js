import { Heuristic } from "./getAiActions.ts";
import testWithTransformations from "./testWithTransformations.test.ts";

Deno.test("open when all others flagged", () => {
  testWithTransformations({
    heuristic: Heuristic.openSingle,
    original: `
      C1F
      `,
    expected: `
      01F
      `,
  });
});
