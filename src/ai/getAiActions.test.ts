import {
  assertEquals,
  assertNotEquals,
} from "https://deno.land/std@0.144.0/testing/asserts.ts";
import getAiActions, { AiAction, Heuristic } from "./getAiActions.ts";
import { stringToState } from "../util/util.ts";

Deno.test("don't include the same heuristic twice", () => {
  const state = `
    #1
    11
  `;

  const actions = getAiActions(stringToState(state), Heuristic.flagSingle);

  const expected: AiAction[] = [
    { op: "flag", pos: [0, 0], heuristic: Heuristic.flagSingle },
  ];
  assertEquals(actions, expected);
});

Deno.test("include all heuristics for the same field", () => {
  const state = `
    FCC#
    1111
  `;

  const actions = getAiActions(stringToState(state));

  const expected: AiAction[] = [
    { heuristic: Heuristic.openSingle, op: "open", pos: [1, 0] },
    { heuristic: Heuristic.openSingle, op: "open", pos: [2, 0] },
    { heuristic: Heuristic.openDouble, op: "open", pos: [1, 0] },
    { heuristic: Heuristic.openBruteForce, op: "open", pos: [1, 0] },
    { heuristic: Heuristic.openBruteForce, op: "open", pos: [2, 0] },
    { heuristic: Heuristic.flagBruteForce, op: "flag", pos: [3, 0] },
    { heuristic: Heuristic.openBruteForceEndgame, op: "open", pos: [1, 0] },
    { heuristic: Heuristic.openBruteForceEndgame, op: "open", pos: [2, 0] },
    { heuristic: Heuristic.flagBruteForceEndgame, op: "flag", pos: [3, 0] },
  ];
  assertEquals(actions, expected);
});

Deno.test("gameOver", async (t) => {
  const assertEmptyWhenGameOver = (
    stateString: string,
    otherHeuristic: Heuristic
  ) => {
    const state = stringToState(stateString);
    const actionsWithoutGameOverHeuristic = getAiActions(state, otherHeuristic);
    const actionsWithImplicitGameOverHeuristic = getAiActions(state);

    assertNotEquals(actionsWithoutGameOverHeuristic, []);
    assertEquals(actionsWithImplicitGameOverHeuristic, []);
  };

  await t.step("don't continue once the game is lost", () => {
    assertEmptyWhenGameOver(
      `
        1X
        CC
      `,
      Heuristic.openSingle
    );
  });

  await t.step("don't continue once all mines have been flagged", () => {
    assertEmptyWhenGameOver(
      `
        1F
        CC
      `,
      Heuristic.openSingle
    );
  });

  await t.step("don't continue once all non-mines have been opened", () => {
    assertEmptyWhenGameOver(`1#1`, Heuristic.flagSingle);
  });
});
