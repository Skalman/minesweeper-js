import { assertEquals } from "https://deno.land/std@0.144.0/testing/asserts.ts";
import { Heuristic } from "./getAiActions.ts";
import { Position, Size, State } from "../models/types.ts";
import {
  applyActions,
  StateAndDebugInfo,
  stateToString,
  stringToState,
} from "../util/util.ts";

const indent = (indent: string, str: string) =>
  indent + str.replaceAll("\n", `\n${indent}`);

const assertStateMatches = (
  original: State,
  actual: StateAndDebugInfo,
  expected: State,
  name: string
) => {
  const expectedString = stateToString(expected);

  if (actual.newStateString !== expectedString) {
    const originalString = stateToString(original);
    console.debug("NAME", name);
    console.debug("ACTIONS", actual.actions);

    console.debug("BEFORE");
    console.debug(indent("  ", originalString));

    console.debug("AFTER");
    console.debug(indent("  ", actual.newStateString));

    console.debug("EXPECTED AFTER");
    console.debug(indent("  ", expectedString));

    assertEquals(actual.newStateString, expectedString);
  }
};

interface TestStepDefinition {
  heuristic: Heuristic;
  original: string;
  expected: string;
}

const testWithTransformations = ({
  heuristic,
  original,
  expected,
}: TestStepDefinition) => {
  const originalState = stringToState(original);
  const expectedState = stringToState(expected);
  const [width, height] = originalState.size;

  type Transpose = (pos: Position) => Position;

  interface Transformation {
    name: string;
    size: Size;
    transpose: Transpose;
  }

  const mirrorX: Transpose = ([x, y]) => [width - 1 - x, y];
  const mirrorY: Transpose = ([x, y]) => [x, height - 1 - y];
  const switchAxes: Transpose = ([x, y]) => [y, x];

  const transformations: Transformation[] = [
    {
      name: "1 original",
      size: [width, height],
      transpose: ([x, y]) => [x, y],
    },
    {
      name: "2 mirror X",
      size: [width, height],
      transpose: mirrorX,
    },
    {
      name: "3 mirror Y",
      size: [width, height],
      transpose: mirrorY,
    },
    {
      name: "4 mirror X and Y",
      size: [width, height],
      transpose: (pos) => mirrorY(mirrorX(pos)),
    },
    {
      name: "5 switch axes",
      size: [height, width],
      transpose: switchAxes,
    },
    {
      name: "6 mirror X, switch axes",
      size: [height, width],
      transpose: (pos) => switchAxes(mirrorX(pos)),
    },
    {
      name: "7 mirror Y, switch axes",
      size: [height, width],
      transpose: (pos) => switchAxes(mirrorY(pos)),
    },
    {
      name: "8 mirror X and Y, switch axes",
      size: [height, width],
      transpose: (pos) => switchAxes(mirrorY(mirrorX(pos))),
    },
  ];

  for (const { name, size, transpose } of transformations) {
    const state = {
      size,
      mines: originalState.mines.map(transpose),
      flags: originalState.flags.map(transpose),
      open: originalState.open.map(transpose),
    };

    const actual = applyActions(state, heuristic);

    const expected = {
      size,
      mines: expectedState.mines.map(transpose),
      flags: expectedState.flags.map(transpose),
      open: expectedState.open.map(transpose),
    };
    assertStateMatches(state, actual, expected, name);
  }
};

export default testWithTransformations;
