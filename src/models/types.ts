import { MineField, MineMap } from "./models.ts";

export type Position = readonly [x: number, y: number];
export type Size = readonly [width: number, height: number];

export interface State {
  size: Size;
  mines: Position[];
  flags: Position[];
  open: Position[];
}

export type HeuristicFunc<T = Set<MineField>> = (map: MineMap) => T;
