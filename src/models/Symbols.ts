enum Symbols {
  flag = "F", // 🚩
  mine = "#", // 🔲 (same as closed)
  closed = "C", // 🔲
  open0 = "0", // 🟦
  open1 = "1", // 1️⃣
  open2 = "2", // 2️⃣
  open3 = "3", // 3️⃣
  open4 = "4", // 4️⃣
  open5 = "5", // 5️⃣
  open6 = "6", // 6️⃣
  open7 = "7", // 7️⃣
  open8 = "8", // 8️⃣
  exploded = "X", // 💥
  badFlag = "Ⅎ", // 🏴
}

export default Symbols;

export type SymbolsRow = Symbols[];
export type SymbolsMap = SymbolsRow[];

export const isOpen = (symbol: Symbols) =>
  (Symbols.open0 <= symbol && symbol <= Symbols.open8) ||
  symbol === Symbols.exploded;

export const isFlag = (symbol: Symbols) =>
  symbol === Symbols.flag || symbol === Symbols.badFlag;

export const isMine = (symbol: Symbols) =>
  symbol === Symbols.mine ||
  symbol === Symbols.flag ||
  symbol === Symbols.exploded;

export const canFlag = (symbol: Symbols) =>
  symbol === Symbols.closed || symbol === Symbols.mine;
