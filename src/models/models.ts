import Symbols from "./Symbols.ts";
import { State, Size, Position } from "./types.ts";
import { dedent } from "../util/util.ts";

interface StableCacheData {
  isMine?: boolean;
  neighbors?: readonly MineField[];
  mineNeighbors?: readonly MineField[];
}

interface DynamicCacheData {
  isOpen?: boolean;
  isFlagged?: boolean;
  closedNeighbors?: readonly MineField[];
  openNeighbors?: readonly MineField[];
  openNeighborsCount?: number;
  notFlaggedNeighbors?: readonly MineField[];
  untouchedNeighbors?: readonly MineField[];
  flaggedNeighborCount?: number;
  toFlagCount?: number;
  toOpenCount?: number;
}

const sortMineFields = (a: MineField, b: MineField) => a.pos - b.pos;

export type GameStatus = "idle" | "playing" | "lose" | "win";

export class MineMap {
  stableCache = new WeakMap<MineField, StableCacheData>();
  dynamicCache = new WeakMap<MineField, DynamicCacheData>();
  #fields: MineField[][] = [];
  #stableData;
  #dynamicData;

  get size() {
    return this.#stableData.size;
  }

  get mines() {
    return this.#stableData.mines;
  }

  /**
   * Excludes zeros, i.e. fields that don't have any neighboring mines.
   */
  get open() {
    this.#dynamicData.open ??= this.openIncludingZeros.filter(
      (x) => x.isMine || x.mineNeighbors.length !== 0
    );
    return this.#dynamicData.open;
  }

  get openIncludingZeros() {
    this.#dynamicData.openIncludingZeros ??= this.#dynamicData.openPositions
      .map(([x, y]) => this.get(x, y))
      .sort(sortMineFields);
    return this.#dynamicData.openIncludingZeros;
  }

  get flags() {
    this.#dynamicData.flags ??= this.#dynamicData.flagPositions
      .map(([x, y]) => this.get(x, y))
      .sort(sortMineFields);
    return this.#dynamicData.flags;
  }

  get toOpen() {
    this.#dynamicData.toOpen ??= this.untouched.filter((x) => !x.isMine);
    return this.#dynamicData.toOpen;
  }

  get toOpenCount() {
    this.#dynamicData.toOpencount ??=
      this.size[0] * this.size[1] -
      this.openIncludingZeros.length -
      this.mines.length;
    return this.#dynamicData.toOpencount;
  }

  get toFlagCount() {
    this.#dynamicData.toFlagCount ??= this.mines.length - this.flags.length;
    return this.#dynamicData.toFlagCount;
  }

  get untouchedCount() {
    this.#dynamicData.untouchedCount ??=
      this.size[0] * this.size[1] -
      this.flags.length -
      this.openIncludingZeros.length;
    return this.#dynamicData.untouchedCount;
  }

  *#calculateUntouched() {
    const [width, height] = this.size;
    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const field = this.get(x, y);
        if (!field.isFlagged && !field.isOpen) {
          yield field;
        }
      }
    }
  }

  get untouched() {
    this.#dynamicData.untouched ??= Array.from(this.#calculateUntouched()).sort(
      sortMineFields
    );
    return this.#dynamicData.untouched;
  }

  #calculateGameStatus(): GameStatus {
    if (this.flags.length === 0 && this.openIncludingZeros.length === 0) {
      return "idle";
    }

    if (
      MineField.intersectionCount(this.openIncludingZeros, this.mines) > 0 ||
      MineField.difference(this.flags, this.mines).length > 0
    ) {
      return "lose";
    }

    if (this.toOpenCount === 0 && this.toFlagCount === 0) {
      return "win";
    }

    return "playing";
  }

  get gameStatus() {
    this.#dynamicData.gameStatus ??= this.#calculateGameStatus();
    return this.#dynamicData.gameStatus;
  }

  constructor(state: State) {
    this.#stableData = this.#createStableData(state);
    this.#dynamicData = this.#createDynamicData(state);
  }

  get(x: number, y: number, sizeForBoundsCheck = this.size): MineField {
    const [width, height] = sizeForBoundsCheck;
    const isWithinBounds = 0 <= x && x < width && 0 <= y && y < height;
    if (!isWithinBounds) {
      throw new RangeError(
        `Out of bounds: attempt to get (${x}, ${y}), but the size is (${width}, ${height})`
      );
    }

    this.#fields[x] ??= [];
    this.#fields[x][y] ??= new MineField(this, x, y);
    return this.#fields[x][y];
  }

  getMultiple(positions: Position[]): readonly MineField[] {
    return positions.map(([x, y]) => this.get(x, y)).sort(sortMineFields);
  }

  #createStableData({ size, mines }: State): {
    size: Size;
    minePositions: State["mines"];
    mines: readonly MineField[];
  } {
    return {
      size: size,
      minePositions: mines,
      mines: mines.map(([x, y]) => this.get(x, y, size)).sort(sortMineFields),
    };
  }

  #createDynamicData({ open, flags }: State): {
    openPositions: State["open"];
    flagPositions: State["flags"];
    open?: readonly MineField[];
    openIncludingZeros?: readonly MineField[];
    flags?: readonly MineField[];
    toOpen?: readonly MineField[];
    toOpencount?: number;
    toFlagCount?: number;
    untouchedCount?: number;
    untouched?: readonly MineField[];
    gameStatus?: GameStatus;
  } {
    return {
      openPositions: open,
      flagPositions: flags,
    };
  }

  updateState(state: State) {
    const isStable =
      this.#stableData.size === state.size &&
      this.#stableData.minePositions === state.mines;

    if (!isStable) {
      this.stableCache = new WeakMap();
      this.#stableData = this.#createStableData(state);
    }

    this.dynamicCache = new WeakMap();
    this.#dynamicData = this.#createDynamicData(state);
  }

  toString() {
    const [width, height] = this.size;

    return Array(height)
      .fill(undefined)
      .map((_, y) =>
        Array(width)
          .fill(undefined)
          .map((_, x) => this.get(x, y).toSymbol())
          .join("")
      )
      .join("\n");
  }

  static createEmpty(width = 0, height = 0) {
    return new MineMap({
      size: [width, height],
      mines: [],
      open: [],
      flags: [],
    });
  }

  static fromString(str: string) {
    const rows = dedent(str).split("\n");
    const width = rows[0].length;
    const height = rows.length;

    const state: State = {
      size: [width, height],
      mines: [],
      flags: [],
      open: [],
    };

    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const symbol = rows[y][x];
        const pos: Position = [x, y];

        switch (symbol) {
          case Symbols.flag:
            state.flags.push(pos);
            state.mines.push(pos);
            break;

          case Symbols.mine:
            state.mines.push(pos);
            break;

          case Symbols.closed:
            break;

          case Symbols.exploded:
            state.mines.push(pos);
            state.open.push(pos);
            break;

          // openX
          default:
            state.open.push(pos);
            break;
        }
      }
    }

    return new MineMap(state);
  }

  toState(): State {
    return {
      size: this.size,
      mines: this.#stableData.minePositions,
      flags: this.#dynamicData.flagPositions,
      open: this.#dynamicData.openPositions,
    };
  }
}

export class MineField
  implements Required<StableCacheData>, Required<DynamicCacheData>
{
  #map;
  readonly x;
  readonly y;
  readonly pos;

  constructor(map: MineMap, x: number, y: number) {
    this.#map = map;
    this.x = x;
    this.y = y;
    this.pos = x * 1000 + y;
  }

  [Symbol.for("Deno.customInspect")](
    _inspect: typeof Deno.inspect,
    opts: Deno.InspectOptions
  ) {
    return "MineField " + Deno.inspect([this.x, this.y], opts);
  }

  toSymbol(): Symbols {
    const { isMine, isFlagged, isOpen, mineNeighbors } = this;

    if (isMine) {
      if (isFlagged) return Symbols.flag;
      if (isOpen) return Symbols.exploded;
      return Symbols.mine;
    }

    if (isFlagged) return Symbols.badFlag;
    if (!isOpen) return Symbols.closed;

    return mineNeighbors.length.toString() as Symbols;
  }

  /**
   * Both arrays are required to be sorted.
   */
  static intersect(
    a: readonly MineField[],
    b: readonly MineField[]
  ): readonly MineField[] {
    const result: MineField[] = [];

    for (let ai = 0, bi = 0; ai < a.length && bi < b.length; ) {
      const aPos = a[ai].pos;
      const bPos = b[bi].pos;

      if (aPos === bPos) {
        result.push(a[ai]);
        ai++;
        bi++;
      } else if (aPos < bPos) {
        ai++;
      } else {
        bi++;
      }
    }

    return result;
  }

  /**
   * Both arrays are required to be sorted.
   */
  static intersectionCount(
    a: readonly MineField[],
    b: readonly MineField[]
  ): number {
    let result = 0;

    for (let ai = 0, bi = 0; ai < a.length && bi < b.length; ) {
      const aPos = a[ai].pos;
      const bPos = b[bi].pos;

      if (aPos === bPos) {
        result++;
        ai++;
        bi++;
      } else if (aPos < bPos) {
        ai++;
      } else {
        bi++;
      }
    }

    return result;
  }

  /**
   * Both arrays are required to be sorted.
   */
  static difference(
    a: readonly MineField[],
    b: readonly MineField[]
  ): readonly MineField[] {
    const result: MineField[] = [];

    for (let ai = 0, bi = 0; ai < a.length && bi <= b.length; ) {
      if (bi === b.length) {
        for (; ai < a.length; ai++) {
          result.push(a[ai]);
        }
        break;
      }

      const aPos = a[ai].pos;
      const bPos = b[bi].pos;

      if (aPos === bPos) {
        ai++;
        bi++;
      } else if (aPos < bPos) {
        result.push(a[ai]);
        ai++;
      } else {
        bi++;
      }
    }

    return result;
  }

  #getStable() {
    let data = this.#map.stableCache.get(this);
    if (!data) {
      data = {};
      this.#map.stableCache.set(this, data);
    }
    return data;
  }

  #getDynamic() {
    let data = this.#map.dynamicCache.get(this);
    if (!data) {
      data = {};
      this.#map.dynamicCache.set(this, data);
    }
    return data;
  }

  get neighbors() {
    const data = this.#getStable();
    data.neighbors ??= this.#calculateNeighbors();
    return data.neighbors;
  }

  #calculateNeighbors() {
    const xThis = this.x;
    const yThis = this.y;
    const [width, height] = this.#map.size;

    const xFrom = Math.max(0, xThis - 1);
    const yFrom = Math.max(0, yThis - 1);
    const xTo = Math.min(width - 1, xThis + 1);
    const yTo = Math.min(height - 1, yThis + 1);

    const result: MineField[] = [];
    for (let x = xFrom; x <= xTo; x++) {
      for (let y = yFrom; y <= yTo; y++) {
        if (x === xThis && y === yThis) continue;
        result.push(this.#map.get(x, y));
      }
    }
    return result.sort(sortMineFields);
  }

  get mineNeighbors() {
    const data = this.#getStable();
    data.mineNeighbors ??= MineField.intersect(this.#map.mines, this.neighbors);
    return data.mineNeighbors;
  }

  get isMine() {
    const data = this.#getStable();
    data.isMine ??= this.#map.mines.includes(this);
    return data.isMine;
  }

  get isOpen() {
    const data = this.#getDynamic();
    data.isOpen ??= this.#map.openIncludingZeros.includes(this);
    return data.isOpen;
  }

  get isFlagged() {
    const data = this.#getDynamic();
    data.isFlagged ??= this.#map.flags.includes(this);
    return data.isFlagged;
  }

  get closedNeighbors() {
    const data = this.#getDynamic();
    data.closedNeighbors ??= this.neighbors.filter((x) => !x.isOpen);
    return data.closedNeighbors;
  }

  get openNeighborsCount() {
    const data = this.#getDynamic();
    data.openNeighborsCount ??=
      this.neighbors.length - this.closedNeighbors.length;
    return data.openNeighborsCount;
  }

  #calculateOpenNeighbors() {
    if (this.isOpen || this.isFlagged) {
      throw new Error("openNeighbors is only available for untouched fields");
    }

    return MineField.intersect(this.neighbors, this.#map.open);
  }

  /**
   * Only available for untouched fields.
   */
  get openNeighbors() {
    const data = this.#getDynamic();
    data.closedNeighbors ??= this.#calculateOpenNeighbors();
    return data.closedNeighbors;
  }

  get notFlaggedNeighbors() {
    const data = this.#getDynamic();
    data.notFlaggedNeighbors ??= this.neighbors.filter((x) => !x.isFlagged);
    return data.notFlaggedNeighbors;
  }

  get untouchedNeighbors() {
    const data = this.#getDynamic();
    data.untouchedNeighbors ??= this.closedNeighbors.filter(
      (x) => !x.isFlagged
    );
    return data.untouchedNeighbors;
  }

  get flaggedNeighborCount() {
    const data = this.#getDynamic();
    data.flaggedNeighborCount ??=
      this.neighbors.length - this.notFlaggedNeighbors.length;
    return data.flaggedNeighborCount;
  }

  get toFlagCount() {
    const data = this.#getDynamic();
    data.toFlagCount ??= this.mineNeighbors.length - this.flaggedNeighborCount;
    return data.toFlagCount;
  }

  get toOpenCount() {
    const data = this.#getDynamic();
    data.toOpenCount ??= this.untouchedNeighbors.length - this.toFlagCount;
    return data.toOpenCount;
  }
}
