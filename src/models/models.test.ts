import { assertEquals } from "https://deno.land/std@0.144.0/testing/asserts.ts";
import { MineField, MineMap } from "./models.ts";
import { State } from "./types.ts";

const emptyState: State = {
  size: [0, 0],
  mines: [],
  flags: [],
  open: [],
};
const smallState: State = {
  size: [2, 2],
  mines: [[0, 1]],
  flags: [[0, 1]],
  open: [[0, 0]],
};

Deno.test("construct empty", () => {
  const map = new MineMap(emptyState);

  const actual = map.flags;

  assertEquals(actual.length, 0);
});

Deno.test("construct small", () => {
  const map = new MineMap(smallState);

  const actual = map.flags;

  assertEquals(actual.length, 1);
  assertEquals(actual[0].x, smallState.flags[0][0]);
  assertEquals(actual[0].y, smallState.flags[0][1]);
});

Deno.test("update state stably", () => {
  const map = new MineMap(smallState);
  map.updateState({
    size: emptyState.size,
    mines: emptyState.mines,
    flags: [],
    open: [],
  });

  const actual = map.flags;

  assertEquals(actual.length, 0);
});

Deno.test("update state unstably", () => {
  const state: State = { ...smallState, mines: [[1, 1]] };
  const map = new MineMap(state);
  map.updateState({ ...state, mines: [[1, 0]] });

  const actual = map.mines;

  assertEquals(actual.length, 1);
  assertEquals([actual[0].x, actual[0].y], [1, 0]);
});

Deno.test("MineField.intersect", () => {
  const map = new MineMap(smallState);

  const array1 = map.getMultiple([
    [0, 1],
    [1, 1],
  ]);
  const array2 = map.getMultiple([
    [0, 0],
    [0, 1],
  ]);

  const actual = MineField.intersect(array1, array2);

  assertEquals(actual.length, 1);
  assertEquals([actual[0].x, actual[0].y], [0, 1]);
});

Deno.test("MineField[Deno.customInspect]", () => {
  const actual = Deno.inspect(MineMap.createEmpty(5, 5).get(3, 4), {
    colors: false,
  });

  assertEquals("MineField [ 3, 4 ]", actual);
});
