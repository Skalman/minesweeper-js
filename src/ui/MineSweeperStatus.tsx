import { React } from "../deps.ts";
import { MineSweeperData } from "./useMineSweeper.ts";

interface MineSweeperStatusProps {
  ms: MineSweeperData;
}

const MineSweeperStatus: React.FC<MineSweeperStatusProps> = ({
  ms: { flagCount, mineCount, untouchedCount, gameStatus },
}) => {
  return (
    <div>
      {gameStatus} {flagCount} / {mineCount} ({untouchedCount} left)
    </div>
  );
};

export default MineSweeperStatus;
