import { Immer, useCallback, useMemo, useState } from "../deps.ts";
import getAiActions, { AiAction } from "../ai/getAiActions.ts";
import { GameStatus, MineField, MineMap } from "../models/models.ts";
import Symbols, { SymbolsMap } from "../models/Symbols.ts";
import { Position, Size, State } from "../models/types.ts";
import { typescriptAssertNever } from "../util/util.ts";

interface InitializationOptions {
  size: Size;
  mineCount: number;
}

export interface MineSweeperData {
  symbols: SymbolsMap;
  highlight: Position[];
  gameStatus: GameStatus;
  aiActions: AiAction[];
  mineCount: number;
  flagCount: number;
  untouchedCount: number;
  dispatch: MineSweeperDispatch;
}

type Action =
  | {
      type: "generate";
      payload?: InitializationOptions;
    }
  | {
      type:
        | "open"
        | "open-neighbors"
        | "flag"
        | "highlight"
        | "highlight-neighbors";
      payload: Position;
    }
  | {
      type: "clear-highlight";
      payload?: undefined;
    };

export type MineSweeperDispatch = (action: Action) => void;

const getFieldsToOpen = (fields: MineField[]) => {
  const fieldsToOpen = new Set<MineField>();

  const recurse = (field: MineField) => {
    if (field.isOpen || fieldsToOpen.has(field) || field.isFlagged) {
      return;
    }

    fieldsToOpen.add(field);

    if (!field.isMine) {
      if (field.mineNeighbors.length === 0) {
        field.untouchedNeighbors.forEach(recurse);
      }
    }
  };

  fields.forEach(recurse);

  return fieldsToOpen;
};

const useMineSweeper = (): MineSweeperData => {
  const [mineMap] = useState(() => MineMap.createEmpty());
  const [mineMapState, setMineMapState] = useState(() => mineMap.toState());
  const [gameStatus, setGameStatus] = useState<GameStatus>("idle");
  const [highlight, setHighlight] = useState<Position[]>([]);
  const [symbols, setSymbols] = useState<SymbolsMap>([]);
  const aiActions = useMemo(() => getAiActions(mineMapState), [mineMapState]);

  const setSymbolsImmer = (recipe: (symbols: SymbolsMap) => void) => {
    setSymbols((symbols) => Immer.produce(symbols, recipe));
  };

  const updateStateImmer = useCallback(
    (recipe: (state: State) => State | void) => {
      const oldState = mineMap.toState();
      const newState = Immer.produce(oldState, recipe);

      mineMap.updateState(newState);
      setMineMapState(newState);

      setGameStatus(mineMap.gameStatus);
      setHighlight([]);
    },
    [mineMap]
  );

  const dispatch = useCallback<MineSweeperDispatch>(
    ({ type, payload }) => {
      switch (type) {
        case "generate": {
          const { size, mineCount } = payload ?? {
            size: mineMap.size,
            mineCount: mineMap.mines.length,
          };

          const [width, height] = size;
          const fieldCount = width * height;
          if (fieldCount < mineCount) {
            throw new RangeError("Bad mine count");
          }

          const mineIndexes = new Set<number>();

          while (mineIndexes.size < mineCount) {
            const rand = Math.floor(Math.random() * fieldCount);
            mineIndexes.add(rand);
          }

          const mines = Array.from(mineIndexes).map<Position>((index) => [
            index % width,
            (index - (index % width)) / width,
          ]);

          updateStateImmer(() => {
            return { size, mines, open: [], flags: [] };
          });

          setSymbols(
            Array(height)
              .fill(undefined)
              .map((_, y) =>
                Array(width)
                  .fill(undefined)
                  .map((_, x) => mineMap.get(x, y).toSymbol())
              )
          );
          break;
        }

        case "open":
        case "open-neighbors": {
          const [x, y] = payload;
          const field = mineMap.get(x, y);

          let fieldsToOpen: Set<MineField>;

          if (type === "open") {
            fieldsToOpen = getFieldsToOpen([field]);
          } else {
            if (field.isOpen && field.toFlagCount == 0) {
              fieldsToOpen = getFieldsToOpen([
                field,
                ...field.untouchedNeighbors,
              ]);
            } else {
              fieldsToOpen = new Set();
            }
          }

          // Check win condition.
          const didWin =
            mineMap.toOpenCount === fieldsToOpen.size && !field.isMine;

          updateStateImmer((draft) => {
            if (fieldsToOpen.size !== 0) {
              if (didWin) {
                draft.flags = draft.mines;
              }

              draft.open.push(
                ...Array.from(fieldsToOpen).map<Position>(({ x, y }) => [x, y])
              );
            }
          });

          setSymbolsImmer((draft) => {
            for (const field of fieldsToOpen) {
              const { x, y } = field;
              draft[y][x] = field.toSymbol();
            }

            if (didWin) {
              for (const { x, y } of mineMap.mines) {
                draft[y][x] = Symbols.flag;
              }
            }
          });
          break;
        }

        case "flag": {
          const [x, y] = payload;
          const field = mineMap.get(x, y);
          if (field.isFlagged || field.isOpen) {
            break;
          }

          // Check win condition.
          const toOpen =
            mineMap.toFlagCount === 1 && field.isMine
              ? mineMap.toOpen
              : undefined;

          updateStateImmer((draft) => {
            draft.flags.push(payload);
            if (toOpen) {
              draft.open.push(...toOpen.map<Position>(({ x, y }) => [x, y]));
            }
          });

          setSymbolsImmer((draft) => {
            draft[y][x] = field.toSymbol();
            if (toOpen) {
              for (const field of toOpen) {
                const { x, y } = field;
                draft[y][x] = field.toSymbol();
              }
            }
          });
          break;
        }

        case "highlight":
          setHighlight([payload]);
          break;

        case "highlight-neighbors": {
          const [x, y] = payload;
          setHighlight([
            payload,
            ...mineMap
              .get(x, y)
              .untouchedNeighbors.map<Position>(({ x, y }) => [x, y]),
          ]);
          break;
        }

        case "clear-highlight":
          setHighlight([]);
          break;

        default:
          typescriptAssertNever(type);
      }
    },
    [mineMap, updateStateImmer]
  );

  return {
    symbols,
    highlight,
    gameStatus,
    aiActions,
    mineCount: mineMap.mines.length,
    flagCount: mineMap.flags.length,
    untouchedCount: mineMap.untouchedCount,
    dispatch,
  };
};

export default useMineSweeper;
