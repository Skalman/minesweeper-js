import { useEffect, useRef, useState } from "../deps.ts";

interface ButtonsDown {
  curLeft: boolean;
  curRight: boolean;
  nextLeft: boolean;
  nextRight: boolean;
  cancel(): void;
}

interface ButtonsMove {
  left: boolean;
  right: boolean;
}

interface ButtonsUp {
  curLeft: boolean;
  curRight: boolean;
  prevLeft: boolean;
  prevRight: boolean;
  cancel(): void;
}

interface UseMouseEventsOptions {
  onMouseDown: (event: MouseEvent, buttons: ButtonsDown) => void;
  onMouseMove: (event: MouseEvent, buttons: ButtonsMove) => void;
  onMouseUp: (event: MouseEvent, buttons: ButtonsUp) => void;
}

const useMouseEvents = (options: UseMouseEventsOptions) => {
  const [elem, setElem] = useState<HTMLElement | null>(null);

  // Use a ref to avoid requiring the use of `useCallback`.
  const optionsRef = useRef(options);
  optionsRef.current = options;

  useEffect(() => {
    if (!elem) return;

    let left = false;
    let right = false;

    const listener = (event: MouseEvent) => {
      const { type, button } = event;
      const curLeft = button === 0 || button === 1;
      const curRight = button === 2 || button === 1;
      const curOptions = optionsRef.current;
      let update = true;

      if (type === "mousedown") {
        curOptions.onMouseDown(event, {
          curLeft: curLeft,
          curRight: curRight,
          nextLeft: left || curLeft,
          nextRight: right || curRight,
          cancel: () => {
            update = left = right = false;
          },
        });
        if (update) {
          left ||= curLeft;
          right ||= curRight;
        }
      } else if (type === "mousemove") {
        curOptions.onMouseMove(event, { left, right });
      } else {
        curOptions.onMouseUp(event, {
          prevLeft: left,
          prevRight: right,
          curLeft: left && curLeft,
          curRight: right && curRight,
          cancel: () => {
            update = left = right = false;
          },
        });
        if (update) {
          left &&= !curLeft;
          right &&= !curRight;
        }
      }
    };

    elem.addEventListener("mousedown", listener);
    elem.addEventListener("mousemove", listener);
    globalThis.addEventListener("mouseup", listener);

    return () => {
      elem.removeEventListener("mousedown", listener);
      elem.removeEventListener("mousemove", listener);
      globalThis.removeEventListener("mouseup", listener);
    };
  }, [elem]);

  return { ref: setElem };
};

export default useMouseEvents;
