import { React } from "../deps.ts";
import Symbols, { isFlag, isMine, isOpen } from "../models/Symbols.ts";

interface MineSweeperFieldProps {
  x: number;
  y: number;
  symbol: Symbols;
  hint: boolean;
  hintNext: boolean;
  highlight: boolean;
}

const MineSweeperField = React.forwardRef<
  HTMLDivElement,
  MineSweeperFieldProps
>(({ x, y, symbol, hint, hintNext, highlight }, ref) => {
  return (
    <div
      key={x}
      ref={ref}
      data-pos={[x, y]}
      data-neighbor-mine-count={symbol}
      className={Object.entries({
        button: true,
        open: isOpen(symbol),
        mine: isMine(symbol),
        flag: isFlag(symbol),
        highlight,
        hint,
        "hint-next": hintNext,
      })
        .filter(([, v]) => v)
        .map(([k]) => k)
        .join(" ")}
    >
      <span>{symbol}</span>
    </div>
  );
});
MineSweeperField.displayName = "MineSweeperField";

export default MineSweeperField;
