import { useEffect, useState } from "../deps.ts";

const useDebouncedValue = <T>(value: T) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    setTimeout(() => setDebouncedValue(value));
  }, [value]);

  return debouncedValue;
};

export default useDebouncedValue;
