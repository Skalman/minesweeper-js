import { React } from "../deps.ts";
import { Heuristic } from "../ai/getAiActions.ts";
import { MineSweeperData } from "./useMineSweeper.ts";

interface MineSweeperToolbarProps {
  ms: MineSweeperData;
  onHintHighlight: (heuristic: Heuristic | undefined) => void;
}

const MineSweeperToolbar: React.FC<MineSweeperToolbarProps> = ({
  ms: { aiActions, dispatch },
  onHintHighlight,
}) => {
  const button = (number: number, text: string, heuristic: Heuristic) => {
    const action = aiActions.find((x) => x.heuristic === heuristic);
    const showHints = () => onHintHighlight(heuristic);
    const hideHints = () => onHintHighlight(undefined);

    return (
      <button
        type="button"
        accessKey={number.toString()}
        disabled={action === undefined}
        onClick={
          action &&
          (() => {
            dispatch({ type: action.op, payload: action.pos });
            showHints();
          })
        }
        onMouseEnter={showHints}
        onFocus={showHints}
        onMouseLeave={hideHints}
        onBlur={hideHints}
      >
        <u>{number}</u> {text}
      </button>
    );
  };

  return (
    <div>
      <button
        type="button"
        accessKey="r"
        onClick={() => dispatch({ type: "generate" })}
      >
        <u>R</u>estart
      </button>
      <button
        type="button"
        accessKey="a"
        disabled={aiActions === undefined || aiActions.length === 0}
        onClick={
          aiActions &&
          (() => {
            dispatch({ type: aiActions[0].op, payload: aiActions[0].pos });
            onHintHighlight(Heuristic.all);
          })
        }
        onMouseEnter={() => onHintHighlight(Heuristic.all)}
        onFocus={() => onHintHighlight(Heuristic.all)}
        onMouseLeave={() => onHintHighlight(undefined)}
        onBlur={() => onHintHighlight(undefined)}
      >
        <u>A</u>utomate
      </button>
      {button(1, "Randomize initial", Heuristic.randomizeInitial)}
      {button(2, "Open single", Heuristic.openSingle)}
      {button(3, "Flag single", Heuristic.flagSingle)}
      {button(4, "Open double", Heuristic.openDouble)}
      {button(5, "Flag double", Heuristic.flagDouble)}
      {button(6, "Open brute force", Heuristic.openBruteForce)}
      {button(7, "Flag brute force", Heuristic.flagBruteForce)}
      {button(8, "Open brute force endgame", Heuristic.openBruteForceEndgame)}
      {button(9, "Flag brute force endgame", Heuristic.flagBruteForceEndgame)}
    </div>
  );
};

export default MineSweeperToolbar;
