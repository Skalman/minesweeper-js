import { React, useEffect, useRef, useState } from "../deps.ts";
import { Heuristic } from "../ai/getAiActions.ts";
import { canFlag } from "../models/Symbols.ts";
import { Position } from "../models/types.ts";
import MineSweeperField from "./MineSweeperField.tsx";
import MineSweeperStatus from "./MineSweeperStatus.tsx";
import MineSweeperSymbolMapDebug from "./MineSweeperSymbolMapDebug.tsx";
import MineSweeperToolbar from "./MineSweeperToolbar.tsx";
import useDebouncedValue from "./useDebouncedValue.ts";
import useMineSweeper from "./useMineSweeper.ts";
import useMouseEvents from "./useMouseEvents.ts";

const MineSweeper: React.FC = () => {
  const mineSweeper = useMineSweeper();
  const { symbols, highlight, gameStatus, aiActions, dispatch } = mineSweeper;
  const elemRefs = useRef(new Map<HTMLElement, Position>());
  const debouncedGameStatus = useDebouncedValue(gameStatus);
  const [showHintHeuristic, setShowHintHeuristic] = useState<Heuristic>();

  useEffect(() => {
    // dispatch({ type: "generate", payload: { size: [3, 3], mineCount: 3 } });
    // dispatch({ type: "generate", payload: { size: [5, 9], mineCount: 10 } });
    // dispatch({ type: "generate", payload: { size: [9, 9], mineCount: 10 } });
    // dispatch({ type: "generate", payload: { size: [16, 16], mineCount: 40 } });
    dispatch({ type: "generate", payload: { size: [30, 16], mineCount: 99 } });
  }, [dispatch]);

  const getPos = (target: EventTarget | null) =>
    elemRefs.current.get(target as HTMLElement);

  const { ref: mapRef } = useMouseEvents({
    onMouseDown: ({ target }, { curRight, nextLeft, nextRight }) => {
      const pos = getPos(target);

      if (!pos) return;

      if (!nextLeft && curRight) {
        const [x, y] = pos;
        const symbol = symbols[y][x];
        if (canFlag(symbol)) {
          dispatch({ type: "flag", payload: pos });
        }
      }

      if (nextLeft && nextRight) {
        dispatch({ type: "highlight-neighbors", payload: pos });
      } else if (nextLeft) {
        dispatch({ type: "highlight", payload: pos });
      }
    },

    onMouseMove: ({ target }, { left, right }) => {
      const pos = getPos(target);

      if (!pos) return;

      if (left && right) {
        return dispatch({ type: "highlight-neighbors", payload: pos });
      } else if (left) {
        dispatch({ type: "highlight", payload: pos });
      }
    },

    onMouseUp: ({ target }, { curLeft, prevLeft, prevRight, cancel }) => {
      const pos = getPos(target);

      if (pos) {
        if (prevLeft && prevRight) {
          dispatch({ type: "open-neighbors", payload: pos });
        } else if (curLeft && !prevRight) {
          dispatch({ type: "open", payload: pos });
        }
      }

      cancel();
      dispatch({ type: "clear-highlight" });
    },
  });

  const hintHighlight =
    showHintHeuristic === undefined
      ? []
      : showHintHeuristic === Heuristic.all
      ? aiActions.map((x) => x.pos)
      : aiActions
          .filter((x) => x.heuristic === showHintHeuristic)
          .map((x) => x.pos);

  return (
    <div
      className={
        debouncedGameStatus === "lose" || debouncedGameStatus === "win"
          ? "gameover"
          : ""
      }
    >
      <MineSweeperToolbar
        ms={mineSweeper}
        onHintHighlight={setShowHintHeuristic}
      />
      <MineSweeperStatus ms={mineSweeper} />
      <div
        ref={mapRef}
        className="map"
        onContextMenu={(e) => e.preventDefault()}
      >
        {symbols.map((row, y) => (
          <div key={y}>
            {row.map((col, x) => (
              <MineSweeperField
                key={x}
                ref={(elem) => elem && elemRefs.current.set(elem, [x, y])}
                x={x}
                y={y}
                symbol={col}
                hint={
                  hintHighlight.find(([hX, hY]) => hX === x && hY === y) !==
                  undefined
                }
                hintNext={
                  hintHighlight.length > 0 &&
                  hintHighlight[0][0] === x &&
                  hintHighlight[0][1] === y
                }
                highlight={
                  highlight.find(([hX, hY]) => hX === x && hY === y) !==
                  undefined
                }
              />
            ))}
          </div>
        ))}
      </div>
      <MineSweeperSymbolMapDebug symbols={symbols} />
    </div>
  );
};

export default MineSweeper;
