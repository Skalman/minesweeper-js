import { React } from "../deps.ts";
import { SymbolsMap } from "../models/Symbols.ts";

interface MineSweeperSymbolMapDebugProps {
  symbols: SymbolsMap;
}

const MineSweeperSymbolMapDebug: React.FC<MineSweeperSymbolMapDebugProps> = ({
  symbols,
}) => {
  return <pre>{symbols.map((row) => row.join("")).join("\n")}</pre>;
};

export default MineSweeperSymbolMapDebug;
