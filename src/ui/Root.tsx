import { React } from "../deps.ts";
import MineSweeper from "./MineSweeper.tsx";

const Root = () => {
  return (
    <React.StrictMode>
      <MineSweeper />
    </React.StrictMode>
  );
};

export default Root;
