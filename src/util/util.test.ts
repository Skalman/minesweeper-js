import { assertEquals } from "https://deno.land/std@0.144.0/testing/asserts.ts";
import { State } from "../models/types.ts";
import { dedent, stateToString } from "./util.ts";

Deno.test("stateToString", () => {
  const state: State = {
    size: [3, 3],
    mines: [
      [0, 0],
      [2, 0],
      [2, 1],
    ],
    flags: [[2, 1]],
    open: [
      [0, 0],
      [1, 0],
      [0, 1],
      [1, 1],
      [0, 2],
      [1, 2],
    ],
  };

  const actual = stateToString(state);

  const expected = dedent(`
    X3#
    13F
    01C
  `);
  assertEquals(actual, expected);
});
