// Copied from https://docs.python.org/3/library/itertools.html#itertools.combinations

export default function* combinations<T>(arr: readonly T[], r: number) {
  const n = arr.length;

  if (r > n) return;

  const indices = Array(r)
    .fill(undefined)
    .map((_, i) => i);

  yield indices.map((i) => arr[i]);

  while (true) {
    let i;
    for (i = r - 1; i >= 0; i--) {
      if (indices[i] !== i + n - r) {
        break;
      }
    }

    if (i === -1) {
      return;
    }

    indices[i]++;

    for (let j = i + 1; j < r; j++) {
      indices[j] = indices[j - 1] + 1;
    }

    yield indices.map((i) => arr[i]);
  }
}
