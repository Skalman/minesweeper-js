import getAiActions, { AiAction, Heuristic } from "../ai/getAiActions.ts";
import { MineMap } from "../models/models.ts";
import { State } from "../models/types.ts";

export const dedent = (str: string) => str.replaceAll(" ", "").trim();

export const stateToString = (state: State) => {
  return new MineMap(state).toString();
};

export const stringToState = (str: string) => {
  return MineMap.fromString(str).toState();
};

export const applyActions = (
  state: State,
  heuristic: Heuristic
): StateAndDebugInfo => {
  state = {
    size: state.size,
    mines: state.mines,
    flags: state.flags.slice(),
    open: state.open.slice(),
  };
  const actions = getAiActions(state, heuristic);

  for (const { op, pos } of actions) {
    if (op === "flag") {
      state.flags.push(pos);
    } else {
      // op === "open"
      state.open.push(pos);
    }
  }

  return {
    actions,
    state,
    newStateString: stateToString(state),
  };
};

export interface StateAndDebugInfo {
  actions: AiAction[];
  state: State;
  newStateString: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const typescriptAssertNever: (type: never) => void = () => {};
