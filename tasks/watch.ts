// @deno-types="https://deno.land/x/esbuild@v0.14.45/mod.d.ts"
import * as esbuild from "https://deno.land/x/esbuild@v0.14.45/mod.js";

const httpPlugin = (storage: Storage): esbuild.Plugin => ({
  name: "http",
  setup(build) {
    build.onResolve({ filter: /^https:\/\// }, (args) => ({
      path: args.path,
      namespace: "http-url",
    }));

    build.onResolve({ filter: /.*/, namespace: "http-url" }, (args) => ({
      path: new URL(args.path, args.importer).toString(),
      namespace: "http-url",
    }));

    build.onLoad({ filter: /.*/, namespace: "http-url" }, async (args) => {
      const cacheKey = `esbuildHttpPlugin-${args.path}`;
      const cached = storage.getItem(cacheKey);

      if (cached !== null) {
        return { contents: cached };
      }

      const response = await fetch(args.path);
      if (response.status !== 200) {
        throw new Error(`Got status ${response.status}`);
      }

      const contents = await response.text();
      storage.setItem(cacheKey, contents);
      return { contents };
    });
  },
});

await esbuild.build({
  absWorkingDir: new URL("../", import.meta.url).pathname,
  entryPoints: ["src/index.ts"],
  outfile: "index.js",
  bundle: true,
  sourcemap: true,
  format: "esm",
  target: ["chrome100", "firefox100", "safari15"],
  logLevel: "info",
  watch: true,
  plugins: [httpPlugin(sessionStorage)],
});
